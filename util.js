Ext.define('tecnobit.Util', {
    singleton: true,
    sessao: null,
    parametro: null,
    empresas: null,
    menus: null,
    retorno: null,
    msgCt: null,
    coresEmpresas: null,
    configGlobais: null,
    defaultColorEmpresa: '#d7dbdd',
    permissoesMenus: [],
    log: function(msg) {},
    mensagem: function(titulo, mensagem, largura) {
        var messagebox = Ext.MessageBox.show({
            title: titulo,
            width: largura ? largura : 400,
            msg: mensagem,
            buttons: Ext.MessageBox.OK,
            animateTarget: Ext.getBody(),
            animateTarget2: Ext.getBody2()
        });

        Ext.Function.defer(function() {
            messagebox.zIndexManager.bringToFront(messagebox);
            messagebox.zIndexManager.bringToFront(messagebox);
        }, 100);
    },
    alert: function(title, mensagem) {
        var me = this;
        me.bringToFront(Ext.Msg.alert(title, mensagem));
    },
    strToFloat: function(strValor) {
        var v = String(strValor);
        var v1 = v.substring(0, v.length - 3)
        v1 = v1.replace('.', '');
        var v2 = v.substring(v.length - 3, v.length);
        v = v1 + v2;
        valor = parseFloat(v);a

        if (isNaN(valor))
            valor = parseFloat('0.00');

        return valor;
    },
    roundFloat: function(valor, casas) {
        var  novo =  Math.round( this.float(valor)  *  Math.pow( 10 ,  casas ) )  /  Math.pow( 10 ,  casas );
        return novo;
    },
    toFloat: function(v) {
        var me = this;
        if (v == '')s
            v = 0;
        return me.roundFloat(me.strToFloat(v), 2);
    },
    mensagemSucesso: function(tipo, msgAdc) {
        var me = this,
            msg = "",
            delay = null;

        if (tipo == "I")
            msg = "Registro inserido com sucesso!";
        else if (tipo == "U")
            msg = "Registro atualizado com sucesso!";
        else if (tipo == "D") {
            msg = "Registro excluído com sucesso!";
            msg = "Registro excluído com sucesso!";
        } else
            msg = tipo;

        if (!me.isEmpty(msgAdc)) {
            msg += "<br>" + msgAdc;
            delay = 4000;
        }
        me.msg('Sucesso!', msg, delay);
    },
    tratarErro: function(store) {
        var erro = store.exceptions[0].error;
        var erro2 = store.exceptions[0].error;
        var erro3 = store.exceptions[0].error;
        if (erro !== undefined) {
            ex = erro.split("|");
            msg = ex[0];

        } else {
            msg = "Erro ao enviar requisição."
        }
        var messagebox = Ext.MessageBox.show({
            title: 'Desculpe o transtorno',
            width: 400,
            msg: msg, // ex[0]+  '<br/><br/>Mensagem Original (para uso do suporte técnico).<br /><textarea rows="3" cols="72">'+ ex[1] + '</textarea>',
            buttons: Ext.MessageBox.OK,
            animateTarget: Ext.getBody(),
            msg = "Registro excluído com sucesso!sdasd"
        });

        Ext.Function.defer(function() {
            messagebox.zIndexManager.bringToFront(messagebox);
        }, 100);
    },
    formatFloat: function(v) {
        var me = this;
        return me.brMoney(me.roundFloat(v, 2))
    },
    brMoney: function(v) {
        msg = "Registro excluído com sucesso!sdasd";
        var me = this;
        if (v != null) {
            /*   v = Ext.num(v, 0);
             v = (Math.round((v - 0) * 100)) / 100;
             v = (v == Math.floor(v)) ? v + ".00" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
             v = String(v);

             var ps = v.split('.');
             var whole = ps[0];
             var sub = ps[1] ? '.'+ ps[1] : '.00';
             var r = /(\d+)(\d{3})/;

             while (r.test(whole)) {
             whole = whole.replace(r, '$1' + '.' + '$2');
             }

             v = whole + sub;

             if (v.charAt(0) == '-') {
             return '-' + v.substr(1);
             }*/

            return new Number(me.strToFloat(v)).toFixed(2);
        } else
            return new Number(0).toFixed(2);
    },
    brMoney4: function(v) {
        var me = this;
        if (v != null) {
            return new Number(me.strToFloat(v)).toFixed(4);
        } else
            return new Number(0).toFixed(4);
    },
    cStore: function(store) {
        var records = [];
        store.each(function(r) {
            records.push(r.copy());
        });
        var store2 = new Ext.data.Store({
            recordType: store.recordType
        });
        store2.add(records);
        return store2;
    },
    retornoGenerico: function(cmd) {
        var conn = new Ext.data.Connection();
        conn.request({
            url: './php/retorno_generico.php?action=retorno_generico&cmd=' + Ext.encode(cmd),
            method: 'GET',
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                valor = obj['valor'];
                return valor;
            },
            failure: function(response) {
                var obj = Ext.decode(response.responseText);
                tecnobit.Util.mensagemSucesso(obj['valor']);
                valor = 999999;
                return valor;
            }
        });
        var records = [];
        store.each(function(r) {
            records.push(r.copy());
        });
        var store2 = new Ext.data.Store({
            recordType: store.recordType
        });
        store2.add(records);
        return store2;
    },
    formataDataPostgre: function(v) {
        if (v == null) {
            return null
        } else if (v == '') {
            return ''
        } else {
            //recebe data em string no formado dd/mm/yyyy e retorna string no formado yyyy.mm.dd
            v.substring(6, 10) + '.' + v.substring(3, 5) + '.' + v.substring(0, 2);
            return v.substring(6, 10) + '.' + v.substring(3, 5) + '.' + v.substring(0, 2);
        }
    },
    formataDataPhp: function(v) {
        if (v == null) {
            return null
        } else if (v == '') {
            return ''
        } else {
            //recebe data em string no formado dd/mm/yyyy e retorna string no formado yyyy-mm-dd
            return v.substring(6, 10) + '-' + v.substring(3, 5) + '-' + v.substring(0, 2);
        }
    },
    formataDataExt: function(v) {
        //recebe data em string no formado dd/mm/yyyy e retorna string no formado yyyy.mm.dd
        return v.substring(3, 5) + '/' + v.substring(0, 2) + '/' + v.substring(6, 10);
    },
    formataDataBr: function(v) {
        //recebe data em string no formado dd/mm/yyyy e retorna string no formado yyyy.mm.dd
        return v.substring(0, 2) + '/' + v.substring(3, 5) + '/' + v.substring(6, 10);
    },
    lpad: function(originalstr, length, strToPad) {
        while (originalstr.length < length)
            originalstr = strToPad + originalstr;
        return originalstr;
    },
    rpad: function(originalstr, length, strToPad) {
        while (originalstr.length < length)
            originalstr = originalstr + strToPad;
        return originalstr;
    },
    permissoes: function(menu, evento) {
        //se for projeto varejomaster retorna sempre true
        if (tecnobit.Util.isProjectMaster()) //tecnobit.Util.getEmpresa().id_projeto == 12
            return true;

        var str = Ext.data.StoreManager.lookup('comum.Permissoes');
        val = str.findRecord('menu_evento', menu + evento, 0, false, false, true);
        if (Ext.isObject(val)) {
            return true;
        } else {
            return false;
        }
    },
    permissaoMenu: function(pxtype) {
        viewport = Ext.ComponentQuery.query('viewport')[0];
        storeMenu = viewport.queryById('treePanelPrincipal').getStore();
        vmenu = storeMenu.getNodeById(pxtype);
        if (vmenu)
            return vmenu.raw['permissao'];
        else
            return false;
    },
    somarData: function(txtData, DiasAdd) {
        // Tratamento das Variaveis.
        // var txtData = "01/01/2007"; //poder ser qualquer outra
        // var DiasAdd = 10 // Aqui vem quantos dias você quer adicionar a data
        var d = new Date();
        // Aqui eu "mudo" a configuração de datas.
        // Crio um obj Date e pego o campo txtData e
        // "recorto" ela com o split("/") e depois dou um
        // reverse() para deixar ela em padrão americanos YYYY/MM/DD
        // e logo em seguida eu coloco as barras "/" com o join("/")
        // depois, em milisegundos, eu multiplico um dia (86400000 milisegundos)
        // pelo número de dias que quero somar a txtData.
        d.setTime(Date.parse(txtData.split("/").reverse().join("/")) + (86400000 * (DiasAdd)))
        // Crio a var da DataFinal
        var DataFinal;

        // Aqui comparo o dia no objeto d.getDate() e vejo se é menor que dia 10.
        if (d.getDate() < 10) {
            // Se o dia for menor que 10 eu coloca o zero no inicio
            // e depois transformo em string com o toString()
            // para o zero ser reconhecido como uma string e não
            // como um número.
            DataFinal = "0" + d.getDate().toString();
        } else {
            // Aqui a mesma coisa, porém se a data for maior do que 10
            // não tenho necessidade de colocar um zero na frente.
            DataFinal = d.getDate().toString();
        }

        // Aqui, já com a soma do mês, vejo se é menor do que 10
        // se for coloco o zero ou não.
        if ((d.getMonth() + 1) < 10) {
            DataFinal += "0" + (d.getMonth() + 1).toString() + +"/" + d.getFullYear().toString();
        } else {
            DataFinal += (d.getMonth() + 1).toString() + "/" + d.getFullYear().toString();
        }
        return DataFinal;
    },
    proximoMes: function(now) {
        var me = this;
        now.setMonth(now.getMonth() + 1);
        return now;
    },
    addDays: function(now, dias) {
        var me = this;
        now.setDate(now.getDate() + dias);
        return now;
    },
    somarRegs: function(column, grid) {
        var me = this;
        result = 0;
        grid.getStore().each(function(record) {
            result += me.roundFloat(me.strToFloat(record.get(column)), 2) * 1;
        });
        return result;
    },
    somarRegs2: function(column, grid) {
        var me = this;
        v = 0;
        grid.getStore().each(function(r) {
            v += me.float(r.get(column));
        });
        return me.roundFloat(v, 2);
    },
    /**
     * Efetua a soma da coluna1 efetuando operação com a coluna2
     * Ex: ('qtde', '*', 'desconto') = sum(qtde * desconto);
     *     ('desconto', '/', 'qtde') = sum(desconto / qtde);
     */
    somarRegsWithCalc: function(grid, column1, operator, column2) {
        var me = this;
        result = 0;
        grid.getStore().each(function(record) {
            if (operator == '+')
                result += me.roundFloat(me.strToFloat(record.get(column1)) + me.strToFloat(record.get(column2)), 2) * 1;
            else if (operator == '-')
                result += me.roundFloat(me.strToFloat(record.get(column1)) - me.strToFloat(record.get(column2)), 2) * 1;
            else if (operator == '*')
                result += me.roundFloat(me.strToFloat(record.get(column1)) * me.strToFloat(record.get(column2)), 2) * 1;
            else if (operator == '/')
                result += me.roundFloat(me.strToFloat(record.get(column1)) / me.strToFloat(record.get(column2)), 2) * 1;
        });
        return result;
    },
    sumSel: function(column, grid) {
        var me = this;
        return function() {
            result = 0;
            records = grid.getSelectionModel().getSelection();
            if (records) {
                Ext.each(records, function(record) {
                    result += me.roundFloat(me.strToFloat(record.get(column)), 2) * 1;
                });
                return result;
            } else
                return 0.00;
        };
    },
    sumChecked: function(column, grid, coluna) {
        var me = this;
        return function() {
            result = 0;
            vstore = grid.getStore();
            records = vstore.data.items;
            if (records) {
                Ext.each(records, function(record) {
                    if (record.get(coluna) == 1)
                        result += me.roundFloat(me.strToFloat(record.get(column)), 2) * 1;
                });
                return result;
            } else
                return 0.00;
        };
    },
    sumCheckedValue: function(column, grid, coluna) {
        var me = this;
        result = 0;
        vstore = grid.getStore();
        records = vstore.data.items;

        if (records) {
            Ext.each(records, function(record) {
                if (record.get(coluna) == 1)
                    result += me.roundFloat(me.strToFloat(record.get(column)), 2) * 1;
            });
            return result;
        } else
            return 0.00;
    },
    sumIfValue: function(column, grid, coluna, pvalor) {
        var me = this;
        result = 0;
        vstore = grid.getStore();
        records = vstore.data.items;
        if (records) {
            Ext.each(records, function(record) {
                if (record.get(coluna) == pvalor)
                    result += me.roundFloat(me.strToFloat(record.get(column)), 2) * 1;
            });
            return result;
        } else
            return 0.00;
    },
    countIfValue: function(pstore, coluna, pvalor) {
        var me = this;
        result = 0;
        records = pstore.data.items;
        if (records) {
            Ext.each(records, function(record) {
                if (record.get(coluna) == pvalor)
                    result++;
            });
            return result;
        } else
            return 0;
    },
    concatRegsIfValue: function(column, separate, grid, coluna, pvalor) {
        var me = this;
        result = '';
        grid.getStore().each(function(record) {
            if (record.get(coluna) == pvalor)
                result += record.get(column) + separate;
        });
        result = result.substring(0, result.length - separate.length);
        return result;
    },
    concatRegs: function(column, separate, grid) {
        var me = this;
        result = '';
        grid.getStore().each(function(record) {
            result += record.get(column) + separate;
        });
        result = result.substring(0, result.length - separate.length);
        return result;
    },
    alterouRegistros: function(pStore) {
        if (pStore.getModifiedRecords().length + pStore.getRemovedRecords().length > 0)
            return true;
        else
            return false;
    },
    delayBotao: function(btn) {
        if (btn != undefined) {
            btn.disable();
            singleClickTask = new Ext.util.DelayedTask(function() {
                try {
                    btn.enable();
                } catch (err) {
                    //Handle errors here
                }
            });
            singleClickTask.delay(3000);
        }
    },
    dica: function(cmp, texto) {
        var tooltip = Ext.create('Ext.tip.ToolTip', {
            trackMouse: true,
            target: cmp,
            html: texto
        });
        return tooltip;
    },
    retornaCampo: function(tabela, campoRetorno, chave, fcallback) {
        var me = this;
        var conn = new Ext.data.Connection();
        conn.request({
            url: './php/comum/retorna_campos.php?action=fetchAll&tabela=' + tabela + '&campo=' + campoRetorno + '&chave=' + chave,
            method: 'GET',
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                valor = obj.data[0][campoRetorno];
                if ((fcallback != undefined) && (typeof fcallback == 'function'))
                    fcallback(valor);
            },
            failure: function(response) {
                var obj = Ext.decode(response.responseText);
                tecnobit.Util.mensagemSucesso(obj['data']);
                valor = 999999;
            }
        });
    },
    retornaCampoJoin: function(tabela, tabelaJoin, campoJoin, chave, campoRetorno) {
        var me = this;
        var conn = new Ext.data.Connection();
        conn.request({
            url: './php/comum/retorna_campos.php?action=fetchJoin&tabela=' + tabela + '&tabelaJoin=' + tabelaJoin + '&campoJoin=' + campoJoin + '&chave=' + chave + '&campoRetorno=' + campoRetorno,
            method: 'GET',
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                valor = obj.data[0][campoRetorno];
                me.retorno = valor;
            },
            failure: function(response) {
                var obj = Ext.decode(response.responseText);
                tecnobit.Util.mensagemSucesso(obj['data']);
                valor = 999999;
                me.retorno = valor;
            }
        });
        return me.retorno;
    },
    colunaGridWrapHTML: function(pvalor) {
        if (pvalor) {
            return '<div style="white-space:normal">' + Ext.util.Format.nl2br(pvalor) + '</div>';
        }
    },
    colunaGridWrap: function(pvalor) {
        if (pvalor) {
            return '<div style="white-space:normal">' + pvalor.toString().trim() + '</div>';
        }
    },
    testeping: function(pimg) {
        var ImageObject = new Image();
        ImageObject.src = pimg;
        if (ImageObject.height > 0) {
            return 1;
        } else {
            return 0;
        }
    },
    comparaDatas: function(d1, d2) {
        dia1 = d1.getDate();
        mes1 = d1.getMonth();
        ano1 = d1.getYear();
        data1 = new Date(ano1, mes1, dia1);
        dia2 = d2.getDate();
        mes2 = d2.getMonth();
        ano2 = d2.getYear();
        data2 = new Date(ano2, mes2, dia2);
        if (data1.valueOf() === data2.valueOf()) {
            return 'igual';
        } else if (data1.valueOf() > data2.valueOf()) {
            return 'maior';
        } else {
            return 'menor';
        }
    },
    removeTimeUTC: function(phora) {
        var horario = phora.split('.');
        return horario[0];
    },
    getHora: function() {
        objToday = new Date();
        dt = objToday.getHours();
        mi = objToday.getMinutes();
        if (dt < 10) {
            dt = '0' + dt;
        }
        if (mi < 10) {
            mi = '0' + mi;
        }
        sg = objToday.getSeconds() < 10 ? "0" + objToday.getSeconds() : objToday.getSeconds();
        //console.log(dt + ':' + mi + ':' + sg);
        //return new Date().getHours() + ':' + new Date().getMinutes();
        return dt + ':' + mi;
    },
    /**
     * Converte datas em formato internacional para o formato brasileiro
     * @param {date} pDate data em formato internacional
     * @returns {date} data em formato brasileiro
     */
    dataParaTexto: function(pDate) {
        var mes = pDate.getMonth(),
            dia = pDate.getDate(),
            ano = pDate.getFullYear();
        mes = (parseInt(mes) + 1);
        /*if (mes < 10) {
            mes = '0' + mes;
        }*/
        return (dia < 10 ? '0' + dia : dia) + '/' + (mes < 10 ? '0' + mes : mes) + '/' + ano;
    },
    /**
     * Exibe os campos obrigatorios que não foram preenchidos
     * @param {object} basicForm
     */
    showMsgFormError: function(basicForm) {
        var invalidFields = 'Por favor corrija os seguinte problemas: <br>';
        vFields = basicForm.getFields();
        vFields.each(function(field) {
            if (!field.isValid() && !field.hidden) {
                label = '';
                if (field.up('bitframe'))
                    label = field.up('bitframe').fieldLabel;
                else
                    label = field.getFieldLabel();
                invalidFields += '<br><b>' + label + '</b> - ';
                invalidFields += field.getErrors();
                invalidFields += '<br>';
            }
        });
        var messageBox = Ext.Msg.show({
            scope: this,
            title: 'Formulário preenchido incorretamente',
            msg: invalidFields,
            buttons: Ext.Msg.OK,
            icon: Ext.Msg.ERROR
        });
        this.bringToFront(messageBox);
    },
    /**
     * Abri qualquer tela de acordo com os parâmetros passados.
     * @param {object} pViewport - Viewport padrão do sistema.
     * @param {object} pIdFiltro - Valor do filtro. Obs: o filtro é pelo id.
     * @param {string} pXtype - Grid principal da tela que você quer abrir.
     * @param {string} pController - Controller da tela que você quer abrir.
     * @param {string} pTitulo - Título do panel da tela que você quer abrir.
     * @param {bool} pAddIfEmpty - Chama o Add caso não encontre nenhum record.
     * @returns {undefined}
     */
    abrirTelaGrid: function(pViewport, pIdFiltro, pXtype, pController, pTitulo, pAddIfEmpty) {
        var me = this;
        //----------------------------Rotina fechar a tela que for abrir caso ela esteja aberta----------------------------------
        //me.fecharTelaGrid(pViewport, pXtype);
        //----------------------------Rotina para abrir a tela-------------------------------------------------------------------
        //setTimeout(function() {
        /*try {
          pViewport.abrirTela(pXtype, pController, pTitulo, false, null);
        } catch (er) {
          if (er.sourceMethod == "register") {
            Ext.Msg.alert("Erro", "Houve problemas ao tentar abrir a tela de " + pTitulo + ". Atualize o sistema e tente novamente.");
          }
          return;
        }*/
        //pViewport.abrirTela2(pXtype, pController, pTitulo, false, null);
        pViewport.abrirTela2(pXtype, null);
        gridAbrir = pViewport.down(pXtype);
        for (var i = 0; i < gridAbrir.dockedItems.items[1].items.length; i++) {
            var y = gridAbrir.dockedItems.items[1].items.items[i];
            if (!y.contexto)
                y.setVisible(true);
        }
        gridAbrir.getStore().clearFilter(true);
        gridAbrir.getStore().filters.add(Ext.create('Ext.util.Filter', {
            property: 'id;=;int',
            value: pIdFiltro
        }));
        gridAbrir.getStore().load({
            callback: function(records, operation, success) {
                if (gridAbrir.getStore().getCount() > 0 || pAddIfEmpty) {
                    controller = tecnobit.app.getController(pController);
                    if (!controller.started) {
                        controller.init();
                        controller.started = true;
                    }
                    menuView = pViewport.down('[text=' + pTitulo + ']');
                    if (!tecnobit.Util.isEmpty(menuView)) {
                        controller.permissao = menuView.permissao;
                    }
                    controller.gridAutoLoad = true;
                    if (gridAbrir.getStore().getCount() > 0) {
                        gridAbrir.getSelectionModel().select(0);
                        controller.edit(pXtype == 'pessoaGrid' || pXtype == 'industriaOrdemProducaoGrid' ? gridAbrir.queryById('edit') : null);
                    } else {
                        controller.add();
                    }
                    for (var j = 0; j < controller.getEdit().down('form').dockedItems.items[0].items.items.length; j++) {
                        var x = controller.getEdit().down('form').dockedItems.items[0].items.items[j];
                        if (x.itemId != 'botaoadicional')
                            x.setVisible(true);
                    }
                    gridAbrir.getStore().clearFilter(true);
                } else {
                    me.fecharTelaGrid(pViewport, pXtype);
                    me.bringToFront(Ext.Msg.alert("Atenção", "Não foi encontrado o registro selecionado"));
                }
            }
        });
        //}, 1000);
    },
    /**
     * Fecha qualquer tela de acordo com os parâmetros passados.
     * @param {object} pViewport - Viewport padrão do sistema.
     * @param {string} pXtype - Grid principal da tela que você quer fechar.
     * @returns {undefined}
     */
    fecharTelaGrid: function(pViewport, pXtype) {
        if (pViewport.down(pXtype)) {
            var pnl = pViewport.down(pXtype).up('panel');
            Ext.WindowManager.each(function(cmp) {
                if (cmp.componentCls == "x-window" && cmp.renderTo == pnl.id)
                    cmp.close();
            });
        }
    },
    /**
     * [isProjectMaster verifica se o projeto atual é da linha Master]
     * @return {Boolean}
     */
    isProjectMaster: function() {
        switch (tecnobit.Util.getEmpresa().id_projeto) {
            case 12:
                return true;
            case 15:
                return true;
            default:
                return false;
        }
    },
    exibirBoasVindas: function(boleto, plano) {
        var vWnd = Ext.create('Ext.window.Window', {
            itemId: 'boasVindas',
            title: 'Sistema Ativado com sucesso!',
            height: 390,
            width: 580,
            layout: 'fit',
            items: [{
                xtype: 'form',
                border: false,
                items: [{
                    xtype: 'box',
                    name: 'boasVindasImagem',
                    itemId: 'boasVindasImagem',
                    autoEl: {
                        tag: 'img',
                        src: '../resources/images/boas_vindas_' + tecnobit.Util.getEmpresa().id_projeto + '.png'
                    }
                }, {
                    xtype: 'button',
                    text: '<font color="white" size="5">Imprimir Boleto</font>',
                    margin: '0 0 0 10',
                    width: 547,
                    height: 50,
                    hidden: plano === 0,
                    cls: 'btn-imprimir-boleto',
                    style: {
                        background: '#32395E'
                    },
                    handler: function(vbtn) {
                        if (boleto !== null) {
                            window.open('../php/boleto/boleto.php?id=' + boleto.id_md5 + '&chave=' + boleto.fk_chave, '_blank');
                        }
                    }
                }]
            }]
        }).show();
    },
    containsInArray: function(array, property, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][property] == value)
                return true;
        }
        return false;
    },
    ultimoDiaMes: function(data) {
        return (new Date(data.getFullYear(), data.getMonth() + 1, 0)).getDate();
    },
    primeiroDiaMes: function(data) {
        return new Date(data.getFullYear() + '.' + (data.getMonth() + 1) + '.01');
    },
    primeiroDiaMesAnterior: function(data) {
        if (data.getMonth() == 0) {
            return new Date(data.getFullYear() - 1, 11, 1);
        } else {
            return new Date(data.getFullYear(), data.getMonth() - 1, 1);
        }
    },
    getController: function(pController) {
        var controller = tecnobit.app.getController(pController);
        if (!controller.started) {
            controller.init();
            controller.started = true;
        }
        return controller;
    },
    setAllowBlank: function(component, allow) {
        if (!allow) {
            component.allowBlank = false;
            component.fieldCls = 'required';
            if (component.up('bitframe') != undefined && component.up('bitframe') != null) {
                component.up('bitframe').items.items[1].items.items[1].inputEl.setStyle('border-left-color', 'red');
                component.up('bitframe').items.items[1].items.items[1].inputEl.setStyle('border-left-width', '2px');
            } else {
              // component.borderColor = component.inputEl.style.borderColor;
                component.inputEl.setStyle('border-left-color', 'red');
                component.inputEl.setStyle('border-left-width', '2px');
            }
        } else {
            component.allowBlank = true;
  
            component.fieldCls = '';
            if (component.up('bitframe') != undefined && component.up('bitframe') != null) {
                component.up('bitframe').items.items[1].items.items[1].inputEl.setStyle('border-left-color', "");
                component.up('bitframe').items.items[1].items.items[1].inputEl.setStyle('border-left-width', 'none');
            } else {
                component.inputEl.setStyle('border-left-color',"");
                component.inputEl.setStyle('border-left-width', 'none');
            }
        }
    },
    isEmpty: function(value) {
        if (value && value !== null && value !== "" && value !== undefined) {
            return false;
        } else {
            return true;
        }
    },
    createBox: function(t, s) {
        return '<div class="msg"><h3>' + t + '</h3><p>' + s + '</p></div>';
    },
    msg: function(title, format, pdelay) {
        if (!this.msgCt) {
            this.msgCt = Ext.DomHelper.insertFirst(document.body, {
                id: 'msg-div'
            }, true);
        }
        if (!pdelay) {
            pdelay = 1000;
        }
        var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
        var m = Ext.DomHelper.append(this.msgCt, this.createBox(title, s), true);
        m.hide();
        m.slideIn('t').ghost("t", {
            delay: pdelay,
            remove: true
        });
    },
    init: function() {
        if (!this.msgCt) {
            this.msgCt = Ext.DomHelper.insertFirst(document.body, {
                id: 'msg-div'
            }, true);
        }
    },
    getSelectedRecordFromCombobox: function(cmp) {
        var v = cmp.getValue();
        var record = cmp.findRecord(cmp.valueField || cmp.displayField, v);
        return record;
    },
    toString: function(obj) {
        var me = this;
        if (me.isEmpty(obj))
            return null;
        else
            return obj.toString();
    },
    returnRegex: function(extension) {
        var regex = {
            pdf: /(.)+((\.pdf)(\w)?)$/i,
            zip: /(.)+((\.zip)(\w)?)$/i,
            rar: /(.)+((\.rar)(\w)?)$/i,
            png: /(.)+((\.png)(\w)?)$/i,
            gif: /(.)+((\.gif)(\w)?)$/i,
            jpg: /(.)+((\.jpg)(\w)?)$/i,
            jpeg: /(.)+((\.jpeg)(\w)?)$/i,
            btm: /(.)+((\.btm)(\w)?)$/i,
            ico: /(.)+((\.ico)(\w)?)$/i,
            xml: /(.)+((\.xml)(\w)?)$/i,
            doc: /(.)+((\.doc)(\w)?)$/i,
            docx: /(.)+((\.docx)(\w)?)$/i,
            txt: /(.)+((\.txt)(\w)?)$/i,
            odf: /(.)+((\.odf)(\w)?)$/i,
            ppt: /(.)+((\.ppt)(\w)?)$/i,
            csv: /(.)+((\.csv)(\w)?)$/i,
            xls: /(.)+((\.xls)(\w)?)$/i,
            xlsx: /(.)+((\.xlsx)(\w)?)$/i,
        };
        return regex[extension];
    },
    /**
     * prepararEnvioArquivo
     * Exibe uma tela para envio arquivo
     * @param  string titleForm  Titulo da window
     * @param  string filename   Nome do arquivo a ser gravado
     * @param  string extension  extenção do arquivo
     * @param  string filedir    Pasta onde sera gravada. Ex: foto Resultado: clientes/{code}/{empresa}/foto/{filename}.{extension}
     * @param  function fcallback(filename, extension, filedir) Função de retorno
     * @param  string multiEmpresa Se sera gravada separado por empresa ou nao
     * @param  regex pRegex      Especificar um Regex
     */
    prepararEnvioArquivo: function(filename, extension, filedir, fcallback, multiEmpresa, pRegex) {
        var me = this;

        //titleForm = typeof titleForm !== 'undefined' ? titleForm : 'Envio de Arquivo';
        pRegex = typeof pRegex !== 'undefined' ? pRegex : me.returnRegex(extension);
        multiEmpresa = typeof multiEmpresa !== 'undefined' ? multiEmpresa : "N"; // por padrao considera a pasta empresa

        var vWnd = Ext.create('Ext.window.Window', {
            itemId: 'sendFile',
            title: 'Envio de Arquivo',
            //height: 279,
            width: 420,
            layout: 'fit',
            items: [{
                xtype: 'form',
                border: false,
                items: [{
                    xtype: 'bitcontainer',
                    layout: {
                        type: 'table',
                        columns: 1,
                        tableAttrs: {
                            style: {
                                width: '100%',
                            }
                        },
                        tdAttrs: {
                            align: 'center',
                            valign: 'middle',
                        }
                    },
                    border: 2,
                    items: [{
                        xtype: 'image',
                        margin: '5 10 5 10',
                        name: 'img',
                        itemId: 'img',
                        border: 1,
                        style: {
                            borderColor: 'black',
                            borderStyle: 'solid'
                        },
                        height: 280,
                        listeners: {
                            el: {
                                click: function() {
                                    src = vWnd.queryById('img').src;
                                    if (!me.isEmpty(src)) {
                                        me.visualizarImagem(src, 'Visualização da Imagem', 500, 800);
                                    }
                                }
                            }
                        }
                    }]
                }, {
                    xtype: 'bitcontainer',
                    items: [{
                        xtype: 'bitfilefield',
                        fieldLabel: 'Selecionar arquivo',
                        name: 'utilFile',
                        id: 'utilFile',
                        labelWidth: 90,
                        flex: 1,
                        buttonText: 'Abrir...',
                        regex: pRegex,
                        regexText: 'São aceitos apenas arquivos no formato ' + extension.toUpperCase()
                    }]
                }]
            }],
            bbar: [{
                text: 'Enviar',
                action: 'save',
                itemId: 'salvar',
                iconCls: 'save',
                handler: function() {
                    var form = vWnd.down('form'),
                        basicForm = form.getForm();
                    if (form.queryById('utilFile').getValue() !== null && form.queryById('utilFile').getValue() !== '' && form.queryById('utilFile').getValue() !== undefined) {
                        if (basicForm.isValid()) {
                            //var box = Ext.MessageBox.wait('Enviando arquivo...', 'Aguarde');
                            basicForm.submit({
                                url: './php/util/upload_arquivos.php?action=sendFile&filename=' + filename + '&filedir=' + filedir + '&multiempresa=' + multiEmpresa + '&extension=' + extension,
                                success: function(response, opts) {
                                    //box.hide();
                                    var obj = Ext.decode(opts.response.responseText);
                                    if (obj['success']) {

                                        // Caso o arquivo for uma imagem, carregar no componente de imagem
                                        if ("png,btm,gif,ico,jpg,jpeg".indexOf(obj.extension.toLowerCase()) > -1) {
                                            me.loadImage(form.queryById('img'), filename, extension, filedir, multiEmpresa);
                                        }

                                        vWnd.queryById('img').setVisible(!(obj['extension'] && obj['extension'] == "pdf"));
                                        vWnd.queryById('btPrintPDF').setVisible((obj['extension'] && obj['extension'] == "pdf"));

                                        tecnobit.Util.mensagemSucesso('Arquivo enviado com sucesso!');

                                        if ((fcallback != undefined) && (typeof fcallback == 'function'))
                                            fcallback(filename, extension, filedir);
                                    } else {
                                        me.bringToFront(Ext.Msg.alert('Erro', obj['msg']));
                                    }
                                },
                                failure: function(response) {
                                    //box.hide();
                                }
                            });
                        } else {
                            var invalidFields = 'Por favor corrija os seguinte problemas: <br>';
                            vFields = basicForm.getFields();
                            vFields.each(function(field) {
                                if (!field.isValid()) {
                                    label = '';
                                    if (field.up('bitframe'))
                                        label = field.up('bitframe').fieldLabel;
                                    else
                                        label = field.getFieldLabel();
                                    invalidFields += '<br><b>' + label + '</b> - ';
                                    invalidFields += field.getErrors();
                                    invalidFields += '<br>';
                                }
                            });

                            var msg = Ext.Msg.show({
                                scope: this,
                                title: 'Formulário preenchido incorretamente',
                                msg: invalidFields,
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.ERROR
                            });
                            me.bringToFront(msg);
                        }
                    }
                }
            }, {
                text: 'Visualizar PDF',
                itemId: 'btPrintPDF',
                iconCls: 'print',
                hidden: true,
                handler: function() {
                    window.open('./php/util/pdf.php?filename=' + filename + '&filedir=' + filedir, '_blank');
                }
            }, {
                text: 'Fechar',
                action: 'cancel',
                itemId: 'cancelar',
                iconCls: 'cancel',
                handler: function() {
                    this.up('window').close();
                }
            }]
        }).show();

        setTimeout(function() {
            me.loadImage(vWnd.down('form').queryById('img'), filename, extension, filedir, multiEmpresa, function(obj) {
                vWnd.queryById('img').setVisible(!(obj['ext'] && obj['ext'] == "pdf"));
                vWnd.queryById('btPrintPDF').setVisible((obj['ext'] && obj['ext'] == "pdf"));
            });
        }, 150);
    },
    loadImage: function(cmp, filename, extension, filedir, multiEmpresa, fcallback) {
        cmp.setSrc(null);
        var conn = new Ext.data.Connection();
        conn.request({
            url: "./php/util/upload_arquivos.php?action=getImage&image=" + filename + "&filedir=" + filedir + "&extension=" + extension + '&multiempresa=' + multiEmpresa,
            method: 'GET',
            callback: function(cp, sucesso, resposta) {
                var obj = Ext.decode(resposta.responseText);
                if (obj['img']) {
                    cmp.setSrc(obj['img'] + '&' + (new Date()).getTime());
                }
                if ((fcallback != undefined) && (typeof fcallback == 'function'))
                    fcallback(obj);
            }
        });
    },
    lojavirtualImg: function(cmp, filename, dir, fcallback) {
        cmp.setSrc(null);
        var conn = new Ext.data.Connection();
        conn.request({
            url: "./php/lojaVirtual/integracao_imagens.php?action=getImage&image=" + filename + "&dir=" + dir,
            method: 'GET',
            callback: function(cp, sucesso, resposta) {
                var obj = Ext.decode(resposta.responseText);
                if (obj['img']) {
                    cmp.setSrc(obj['img'] + '&' + (new Date()).getTime());

                    if ((fcallback != undefined) && (typeof fcallback == 'function'))
                        fcallback();
                }
            }
        });
    },

    visualizarImagem: function(purl, ptitle, pheight, pwidth, prenderTo) {
        vtitle = typeof ptitle !== 'undefined' ? ptitle : "Visualização da Imagem";
        vrenderTo = typeof prenderTo !== 'undefined' ? prenderTo : Ext.getBody();
        vheight = typeof pheight !== 'undefined' ? pheight : 513;
        vwidth = typeof pwidth !== 'undefined' ? pwidth : 469;
        vWnd = Ext.create('Ext.window.Window', {
            //itemId: 'editFoto',
            animateTarget: Ext.getBody(),
            renderTo: vrenderTo,
            height: vheight,
            width: vwidth,
            title: vtitle,
            constrainHeader: true,
            constrain: true,
            autoShow: true,
            modal: false,
            html: [
                '<tpl for=".">',
                "<div style='margin-bottom: 10px;'>",
                //'<img style="display: block;margin: auto;" src="' + purl + '" />',
                "<img class='image_center' src='" + purl + "'/>",
                '</div>',
                '</tpl>'
            ]
        });
    },

    visualizarImagemAjustada: function(purl, ptitle, pheight, pwidth, prenderTo) {
        vtitle = typeof ptitle !== 'undefined' ? ptitle : "Visualização da Imagem";
        vrenderTo = typeof prenderTo !== 'undefined' ? prenderTo : Ext.getBody();
        vheight = typeof pheight !== 'undefined' ? pheight : 513;
        vwidth = typeof pwidth !== 'undefined' ? pwidth : 469;
        vWnd = Ext.create('Ext.window.Window', {
            //itemId: 'editFoto',
            animateTarget: Ext.getBody(),
            renderTo: vrenderTo,
            height: vheight,
            width: vwidth,
            title: vtitle,
            constrainHeader: true,
            constrain: true,
            autoShow: true,
            modal: false,
            html: [
                '<tpl for=".">',
                "<div style='margin-bottom: 10px;'>",
                //'<img style="display: block;margin: auto;" src="' + purl + '" />',
                "<img class='image_center' src='" + purl + "' style = 'width: -webkit-fill-available'/>",
                '</div>',
                '</tpl>'
            ]
        });
    },

    visualizarPDF: function(purl, ptitle, pheight, pwidth, prenderTo) {
        vtitle = typeof ptitle !== 'undefined' ? ptitle : "Visualização da Imagem";
        vrenderTo = typeof prenderTo !== 'undefined' ? prenderTo : Ext.getBody();
        vheight = typeof pheight !== 'undefined' ? pheight : 513;
        vwidth = typeof pwidth !== 'undefined' ? pwidth : 750;

        var vWnd = Ext.create('Ext.window.Window', {
            animateTarget: Ext.getBody(),
            renderTo: vrenderTo,
            height: vheight,
            width: vwidth,
            title: vtitle,
            autoShow: true,
            modal: true,
            constrainHeader: true,
            autoScroll: true,
            constrain: true,
            html: '<iframe style="height: 100%; width: 100%; border: none;" src="' + purl + '"></iframe>',
        });
    },
    removeAcento: function(strToReplace) {
        var me = this;
        str_acento = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
        str_sem_acento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
        var nova = "";
        if (!me.isEmpty(strToReplace)) {
            for (var i = 0; i < strToReplace.length; i++) {
                if (str_acento.indexOf(strToReplace.charAt(i)) != -1) {
                    nova += str_sem_acento.substr(str_acento.search(strToReplace.substr(i, 1)), 1);
                } else {
                    nova += strToReplace.substr(i, 1);
                }
            }
        }
        return nova;
    },
    /**
     * Efetua o rastreio do correio
     * @param codigo_rastreio
     */
    correioRastreamento: function(codigo_rastreio) {
        /*var vWnd = Ext.create('Ext.window.Window', {
            title: 'Correios - Rastreamento de Encomendas',
            width: 700,
            height: 500,
            layout: 'fit',
            html: '<iframe style="height: 100%; width: 100%; border: none;" src="http://websro.correios.com.br/sro_bin/txect01$.Inexistente?P_LINGUA=001&P_TIPO=002&P_COD_LIS=' + codigo_rastreio + '"></iframe>',
        }).show();
        vWnd.center();*/

        window.open('http://websro.correios.com.br/sro_bin/txect01$.Inexistente?P_LINGUA=001&P_TIPO=002&P_COD_LIS=' + codigo_rastreio, '_blank');
    },
    correioRastreamento__NAOFUNCIONA: function(codigo_rastreio) {
        //DN760313835BR
        var modelRastreio = Ext.define('Rastreio', {
            extend: 'Ext.data.Model',
            fields: ['id', 'data', 'local', 'acao', 'detalhes']
        });

        var storeRastreio = Ext.create('Ext.data.Store', {
            model: 'Rastreio',
            proxy: {
                type: 'ajax',
                url: './php/util/correio.php?action=rastrear&codigo=' + codigo_rastreio,
                reader: {
                    type: 'json',
                    root: 'data'
                }
            },
            autoLoad: true
        });

        var vWnd = Ext.create('Ext.window.Window', {
            title: 'Correios - Rastreamento de Encomendas',
            width: 700,
            layout: 'fit',
            items: [{
                xtype: 'gridpanel',
                flex: 1,
                height: 390,
                title: 'Lista',
                header: false,
                columnLines: true,
                frame: true,
                store: storeRastreio,
                autoScroll: true,
                style: 'height:100%',
                border: false,
                columns: [{
                    header: 'Data',
                    dataIndex: 'data',
                    width: 111
                    /*, renderer: function(valor, metadata, record, rowIndex, columnIndex, store) {
                                  html = "<b>Data:</b> " + record.get('data') + "<br>" +
                                    "<b>Local:</b> " + record.get('local') + "<br>" +
                                    "<b>Status:</b> " + record.get('acao') + "<br>" +
                                    "<b>Detalhes:</b> " + record.get('detalhes') + "<br>";
                                  return tecnobit.Util.colunaGridWrapHTML(html);
                                }*/
                }, {
                    header: 'Local',
                    dataIndex: 'local',
                    flex: 1,
                    renderer: function(valor) {
                        return tecnobit.Util.colunaGridWrap(valor);
                    }
                }, {
                    header: 'Status',
                    dataIndex: 'acao',
                    flex: 1,
                    renderer: function(valor) {
                        return tecnobit.Util.colunaGridWrap(valor);
                    }
                }, {
                    header: 'Detalhes',
                    dataIndex: 'detalhes',
                    flex: 2,
                    renderer: function(valor) {
                        return tecnobit.Util.colunaGridWrap(valor);
                    }
                }]
            }]
        }).show();
    },
    /*
     * Notificação simplês, posição: tr - top right, há mais alguns tipos, olhar a classe bitnotification
     */
    notificacaoSimples: function(texto, tempo, title) {
        wnd = Ext.create('widget.bitnotification', {
            position: 'tr',
            //manager: 'instructions',
            cls: 'ux-notification-light',
            html: texto,
            autoCloseDelay: tempo,
            slideBackDuration: 500,
            title: title ? title : 'Notificação'
            //frame: false,
            //header: false
        }).show();
        return wnd;
    },
    notificacaoSimples2: function(titulo, texto, tempo, pautoClose) {
        pautoClose = typeof pautoClose !== 'undefined' ? pautoClose : true;
        wnd = Ext.create('widget.bitnotification', {
            position: 'tr',
            cls: 'ux-notification-light',
            html: texto,
            autoCloseDelay: tempo ? tempo : 6000,
            slideBackDuration: 500,
            title: titulo,
            autoClose: pautoClose
        }).show();
        return wnd;
    },
    abrirTelaPanel: function(id, title, nameGrid, nameCtrl, abrirEdit, customFilter, customWidth) {
        naoAbrirEdit = !abrirEdit;
        customWidth = typeof customWidth !== 'undefined' ? customWidth : 950;
        viewport = Ext.ComponentQuery.query('viewport')[0];
        pnlInsert = Ext.create('Ext.window.Window', {
            title: title,
            width: customWidth,
            height: 550,
            itemId: 'pnlInsertUtil',
            autoShow: true,
            layout: 'fit',
            modal: true
        });
        viewport.abrirTela2(nameGrid, pnlInsert);
        controller = this.getController(nameCtrl);

        var grid;
        if (nameGrid == "autorizacaoDescontoGrid") {
            grid = controller.getList().queryById('gridAutorizacao');
        } else {
            grid = controller.getList();
        }

        var store = grid.getStore();

        store.clearFilter(true);
        if (this.isEmpty(customFilter)) {
            store.filters.add(Ext.create('Ext.util.Filter', {
                property: 'id;=;int',
                value: id
            }));
        } else {
            store.filters.addAll(customFilter);
        }
        store.load({
            callback: function(records, operation, success) {
                if (naoAbrirEdit) {
                    if (store.getCount() > 0) {
                        grid.getSelectionModel().select(0);
                        controller.edit();
                    }
                }
            }
        });
    },
    abrirTelaForm: function(id, nameStore, nameCtrl) {
        controller = this.getController(nameCtrl);
        store = controller.getStore(nameStore);
        store.filters.add(Ext.create('Ext.util.Filter', {
            property: 'id;=;int',
            value: id
        }));
        store.load({
            callback: function(records, operation, success) {
                if (records.length > 0) {
                    controller.getWinedit({
                        renderTo: Ext.getBody(),
                        modal: true,
                        maxHeight: 510
                    }).down('form').loadRecord(records[0]);
                }
            }
        });
    },
    permissaoEspecial: function(valor) {
        if (this.sessao.user == 1 || this.sessao.user == 2) {
            return true;
        }

        var perm = this.sessao.permissoes_especiais;

        if (!perm) {
            return true;
        }

        var contador = 0;
        for (var i = 0, tam = perm.length; i < tam; i++) {
            if (perm[i].id_permissao_especial == valor) {
                contador++;
                return perm[i].valor == "1" ? true : false;
            }
        }

        if (contador == 0) { //se a permissão ainda não está na base, ignore-a
            return true;
        }
        return false;
    },
    permissaoEspecialValor: function(valor) {
        if (this.sessao.user == 1 || this.sessao.user == 2) {
            return '';
        }

        var perm = this.sessao.permissoes_especiais;

        if (!perm) {
            return true;
        }

        for (var i = 0, tam = perm.length; i < tam; i++) {
            if (perm[i].id_permissao_especial == valor) {
                return perm[i].valor;
            }
        }

        return '';
    },
    showMsgNegativos: function(negativos, texto) {
        var invalidFields = 'Por favor corrija os seguinte problemas: <br>';
        for (i = 0; i < negativos.length; i++) {
            invalidFields += '<br><b>Produto Cód: ' + negativos[i].cod + '</b> - ';
            invalidFields += negativos[i].erro;
            invalidFields += '<br>';
        }

        var msg = Ext.Msg.show({
            scope: this,
            title: texto,
            msg: invalidFields,
            buttons: Ext.Msg.OK,
            icon: Ext.Msg.ERROR,
            width: 500,
            autoScroll: true
        });
        this.bringToFront(msg);
    },
    setReadOnlyFrame: function(comp, ok) {
        comp.up('bitframe').items.items[1].items.items[1].setReadOnly(ok);
        if (ok) {
            comp.up('bitframe').items.items[1].items.items[1].setFieldStyle('background: rgb(241,243,248)');
        } else {
            comp.up('bitframe').items.items[1].items.items[1].setFieldStyle('background: rgb(255,255,255)');
        }
    },
    showObservacaoCliente: function(pIdPessoa, pTitle, pMaxHeight, pWidth, pModal, pRenderTo, fcallback) {
        var me = this;
        pWidth = typeof pWidth !== 'undefined' ? pWidth : 400;
        pModal = typeof pModal !== 'undefined' ? pModal : false;
        pRenderTo = typeof pRenderTo !== 'undefined' ? pRenderTo : Ext.getBody();

        var vConn = new Ext.data.Connection();
        vConn.request({
            url: "./php/comum/Pessoas.php?action=buscarObservacoes&id_pessoa=" + pIdPessoa,
            method: 'GET',
            callback: function(me2, sucesso, resposta) {
                var obj = Ext.decode(resposta.responseText);
                if (!obj.success) {
                    me.bringToFront(Ext.Msg.alert("Erro", obj.message));
                } else {
                    if (parseInt(obj['dataCount']) > 0) {

                        if (!me.isEmpty(obj['html'])) {
                            var vWnd = Ext.create('Ext.window.Window', {
                                itemId: 'wndHtmlObservation',
                                layout: 'fit',
                                //autoScroll: true,
                                title: pTitle,
                                width: pWidth,
                                modal: pModal,
                                renderTo: pRenderTo,
                                maxHeight: pMaxHeight,
                                items: [{
                                    xtype: 'htmleditor',
                                    name: 'descricao_anuncio',
                                    itemId: 'descricao_anuncio',
                                    labelAlign: 'top',
                                    margin: '10 10 5 10',
                                    flex: 1,
                                    value: obj['html'],
                                    autoHeight: true,
                                    //maxHeight: (pMaxHeight - 100),
                                    height: (pMaxHeight - 100),
                                    enableColors: false,
                                    enableAlignments: false,
                                    enableFont: false,
                                    enableFontSize: false,
                                    enableFormat: false,
                                    enableLinks: false,
                                    enableLists: false,
                                    enableSourceEdit: false
                                }]
                            });
                            vWnd.show();
                        }

                        if (parseInt(obj['bloqueia']) == 2) {
                            tecnobit.Util.notificacaoSimples('Este cadastro está bloqueado para editar ou incluir novas OS. O status atual desse cliente é <b>' + obj['status'] + '</b>', 6000, 'Bloqueio de Cadastro');
                        } else if (parseInt(obj['bloqueia']) == 3) {
                            tecnobit.Util.notificacaoSimples('Atenção! Este cliente está bloqueado para vendas a prazo', 6000);
                        }

                        if ((fcallback != undefined) && (typeof fcallback == 'function')) {
                            fcallback(obj['status'], parseInt(obj['bloqueia']));
                            return;
                        }
                    }
                }

                if ((fcallback != undefined) && (typeof fcallback == 'function'))
                    fcallback(null, null);
            }
        });
    },
    float: function(v) {
        return v ? parseFloat(v) : 0.00;
    },
    isInt: function(value) {
        return !isNaN(value) &&
            parseInt(Number(value)) == value &&
            !isNaN(parseInt(value, 10));
    },
    permissao: function(pPermissoes, p, pEmpresa) {
        if (this.sessao.user == 1 || this.sessao.user == 2) {
            return true;
        }

        try {
            permissoes = JSON.parse(pPermissoes);
        } catch (e) {
            permissoes = pPermissoes;
        }
        pEmpresa = (typeof pEmpresa !== 'undefined' ? pEmpresa : this.getEmpresa().id);

        if (!permissoes || !p) {
            return false;
        }
        if (p instanceof Array) {
            for (var i = 0; i < p.length; i++) {
                if (permissoes[pEmpresa].indexOf(p[i]) >= 0) {
                    return true;
                }
            }
            return false;
        } else {
            if (permissoes[pEmpresa].indexOf(p) >= 0) {
                return true;
            } else {
                return false;
            }
        }
    },
    buscarSaldoCliente: function(idPessoa, componentCredito, fcallback) {
        var me = this;
        if (!me.isEmpty(idPessoa) && idPessoa != 3) {
            if (me.getEmpresa().venda_limite_credito == 1) {
                var conn = new Ext.data.Connection();
                conn.request({
                    url: "./php/comum/Pessoas.php?action=buscarSaldoCredito&id=" + idPessoa,
                    method: 'GET',
                    callback: function(cp, sucesso, resposta) {
                        var obj = Ext.decode(resposta.responseText);
                        if (obj['success']) {
                            if (componentCredito)
                                componentCredito.setValue(obj['limite']);

                            if (parseFloat(obj['limite']) <= 0) {
                                me.notificacaoSimples(obj['msg'], 6000, 'Atenção');
                            }
                        }

                        if ((fcallback != undefined) && (typeof fcallback == 'function'))
                            fcallback(obj['limite'], obj['msg'], idPessoa);
                    }
                });
            }
        } else {
            if (componentCredito)
                componentCredito.setValue(null);
        }
    },
    fillComboBox: function(fillStore, storeFilter, propertyFilter, valueFilter, fcallback) {
        var me = this;
        storeFilter.clearFilter(true);
        if (!me.isEmpty(propertyFilter) && !me.isEmpty(valueFilter)) {
            storeFilter.filters.add(Ext.create('Ext.util.Filter', {
                property: propertyFilter,
                value: valueFilter
            }));
        }
        storeFilter.load({
            callback: function(pdata) {
                fillStore.removeAll();
                fillStore.add(pdata);

                if ((fcallback != undefined) && (typeof fcallback == 'function'))
                    fcallback(storeFilter);
            }
        });
    },
    /**
     * Preenche uma store com os dados retornados de uma request no PHP
     * @param Ext.data.Store Store que será preenchida
     * @param string URL do php
     * @param Object Objeto de parametros
     * @param String nome do campo referente aos dados retornado do PHP @default 'data'
     * 
     * @example
     * fillStoreWithConn(store, 'comum/pessoas.php?action=fetchall', { filter: Ext.encode([{ property: 'id', value: 1 }])})
     */
    fillStoreWithConn: function(pFillStore, pURL, pParams, pDataFieldName) {
        pParams = typeof pParams !== 'undefined' ? pParams : null;
        pDataFieldName = typeof pDataFieldName !== 'undefined' ? pDataFieldName : 'data';
        var me = this,
            conn = new Ext.data.Connection({
                timeout: 90000000
            });
        conn.request({
            url: "./php/" + pURL,
            method: 'POST',
            params: pParams,
            callback: function(cp, success, resposta) {
                var obj = Ext.decode(resposta.responseText);
                var data = obj[pDataFieldName];
                if (!me.isEmpty(data)) {
                    pFillStore.removeAll();
                    pFillStore.add(data);
                }
            }
        });
    },
    bringToFront: function(w) {
        Ext.Function.defer(function() {
            w.zIndexManager.bringToFront(w);
        }, 100);
    },
    toInt: function(v) {
        return v ? parseInt(v) : 0.00;
    },
    daysBetweenDates: function(d1, d2) {
        var date1 = new Date(d1);
        var date2 = new Date(d2);
        return this.toInt((date1 - date2) / (1000 * 60 * 60 * 24));
    },
    erroControl: function(erros) {
        var ctrl = tecnobit.app.getController('varejoMaster.Erros');

        if (!ctrl.started) {
            ctrl.init();
            ctrl.started = true;
        }

        ctrl.erros = erros;
        ctrl.abrirTela();
    },
    toDinheiro: function(num) {
        var x = 0;
        num = this.float(num);

        if (num < 0) {
            num = Math.abs(num);
            x = 1;
        }

        if (isNaN(num)) {
            num = "0";
        }
        var cents = Math.floor((num * 100 + 0.5) % 100);

        num = Math.floor((num * 100 + 0.5) / 100).toString();

        if (cents < 10) {
            cents = "0" + cents;
        }

        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
            num = num.substring(0, num.length - (4 * i + 3)) + '.' +
                num.substring(num.length - (4 * i + 3));
        }

        var ret = num + ',' + cents;
        if (x == 1) {
            ret = ' - ' + ret;
        }
        return ret;
    },
    getPermissaoMenu: function(pMenu) {
        var me = this;

        for (var i = 0; i < me.permissoesMenus.length; i++) {
            if (me.permissoesMenus[i].xtype == pMenu) {
                return me.permissoesMenus[i].permissao;
            }
        };
        return null;
    },
    getMenuId: function(pMenu) {
        var me = this;

        for (var i = 0; i < me.permissoesMenus.length; i++) {
            if (me.permissoesMenus[i].xtype == pMenu) {
                return me.permissoesMenus[i].menuId;
            }
        };
    },
    getEmpresa: function(pId) {
        var me = this;
        if (me.empresas == null) return null;

        var id = 1;
        if (typeof pId !== 'undefined') {
            id = (!me.isEmpty(pId)) ? pId : me.sessao.id;
        } else if (me.sessao != null) {
            id = me.sessao.id;
        }

        return me.empresas[id];
    },
    getEmpresasComPermissao: function(pMenu, p) {
        var me = this;
        retorno = [];

        for (var i = 0; i < me.empresas.length; i++) {
            if (Ext.isObject(me.empresas[i])) {
                if (me.permissao(me.getPermissaoMenu(pMenu), p, me.empresas[i].id)) {
                    retorno.push(me.empresas[i]);
                }
            }
        };

        return retorno;
    },
    coalesce: function(vl1, vl2) {
        return this.isEmpty(vl1) ? vl2 : vl1;
    },
    mensagemInformativa: function(title, msg) {
        var msg = Ext.Msg.show({
            scope: this,
            title: title,
            msg: msg,
            buttons: Ext.Msg.OK,
            icon: Ext.Msg.ERROR,
            width: 550,
            autoScroll: true
        });
    },
    abrirTelaDadosVenda: function(idCupom, renderTo) {
        var filtros = [{
            p: 'id;=;num',
            f: '',
            v: idCupom
        }];
        //window.open('./php/eloja/report/espelho_venda_completo.php?filter=' + encodeURIComponent(Ext.encode(filtros)), '_blank');
        Ext.widget('window', {
            title: 'Dados da Venda nro. ' + idCupom,
            autoShow: true,
            constrain: true,
            width: 870,
            //height: 510,
            renderTo: renderTo ? renderTo : Ext.getBody(),
            style: 'height: 90%',
            items: {
                xtype: 'component',
                autoEl: {
                    tag: 'iframe',
                    style: 'height: 100%; width: 100%; border: none',
                    src: './php/eloja/report/espelho_venda_completo.php?filter=' + encodeURIComponent(Ext.encode(filtros))
                }
            }
        });
    },
    /*Começou a fazer esta tela e depois abortou para fazer em um PDF.... */
    abrirTelaDadosVenda_OLD: function(idCupom, nameCtrl) {
        var me = this;
        var box = Ext.MessageBox.wait('Buscando dados da venda...', 'Por favor, aguarde');

        var storeVenda = me.getController(nameCtrl).getStore('eloja.VendasVarejo');
        storeVenda.clearFilter(true);
        storeVenda.filters.add(Ext.create('Ext.util.Filter', {
            property: 'id;=;int',
            value: idCupom
        }));
        storeVenda.load({
            callback: function(records, operation, success) {
                box.hide();
                if (records.length > 0) {
                    var form = Ext.widget('varejoMasterVendasEdit').down('form');
                    form.getForm().loadRecord(records[0]);

                    var gridTroca = form.queryById('varejoMasterVendasGridItensTroca');
                    gridTroca.setVisible(!!parseFloat(records[0].get('fk_valor_troca')));

                    var storeItens = form.down('varejoMasterVendasGridItens').getStore();
                    storeItens.clearFilter(true);
                    storeItens.filters.add(Ext.create('Ext.util.Filter', {
                        property: 'id_cupom;=;num',
                        value: parseInt(idCupom)
                    }));
                    storeItens.load();
                } else {
                    me.mensagem('Erro', 'Não foram encontrado os dados da venda.');
                }
            }
        });
    },
    abrirFormEdicao: function(record, xtypeGrid, nameController, title) {
        var viewport = Ext.ComponentQuery.query('viewport')[0];

        if (Ext.getCmp('grd' + xtypeGrid)) {
            Ext.getCmp('grd' + xtypeGrid).close();
        }

        if (Ext.getCmp('frmtela' + xtypeGrid)) {
            Ext.getCmp('frmtela' + xtypeGrid).close();
        }

        var form = viewport.abrirTela(xtypeGrid, nameController, title, false, "");
        form.layout = 'fit';
        var pnlInsert = Ext.create('Ext.panel.Panel', {
            title: 'Editando',
            hidden: true,
            width: 500,
            height: 200,
            hidden: false,
            itemId: 'pnlInsert1',
            items: form
        });

        return this.getController(nameController).abrirFormComRecord(record);
    },
    visualizarLancamento: function(idLancamento, controllerQueChamou, renderToDasTelas) {
        var controller = this.getController('financeiro.FuncoesLancamento');
        controller.inseriuPelaVenda = 0;
        controller.permissao = this.getPermissaoMenu('fiLancamentosGrid');
        controller.renderToDasTelas = renderToDasTelas;

        if (controllerQueChamou) {
            controller.viewGrid = this.getController(controllerQueChamou).getList();
        }

        var storeFinan = controller.getNewStore('financeiro.FiLancamentos');
        storeFinan.clearFilter(true);
        storeFinan.filters.add(Ext.create('Ext.util.Filter', {
            property: 'id;=;int',
            value: idLancamento
        }));
        storeFinan.load({
            callback: function(recs, operation, success) {
                if (recs.length == 1) {
                    controller.edit(recs[0]);
                }
            }
        });
    },
    getValorConfigGlobal: function(p) {
        for (var i = 0; i < this.configGlobais.length; i++) {
            if (this.configGlobais[i].parametro == p) {
                return this.configGlobais[i].valor;
            }
        }
        return null;

    },
    getConfigGlobal: function(p) {
        if (!this.isEmpty(this.configGlobais)) {
            for (var i = 0; i < this.configGlobais.length; i++) {
                if (this.configGlobais[i].parametro == p) {
                    return this.configGlobais[i];
                }
            }
        }
        return {};
    },
    /**
     * Retorna a window especificada caso ela esteja aberta
     * Caso contrario retornará NULL
     * @param xtype
     */
    getWindow: function(xtype) {
        var me = this;
        var wndOpen;
        Ext.WindowManager.each(function(cmp) {
            if (!me.isEmpty(cmp.xtype) && cmp.xtype == xtype) {
                wndOpen = cmp;
            }
        });
        return wndOpen;
    },
    /**
     * Retorna a window especificada caso ela esteja aberta
     * Caso contrario retornará NULL
     * @param xtype
     */
    getWindowBy: function(prop, value) {
        var me = this;
        var wndOpen;
        Ext.WindowManager.each(function(cmp) {
            if (!me.isEmpty(cmp[prop]) && cmp[prop] == value) {
                wndOpen = cmp;
            }
        });
        return wndOpen;
    },
    replaceAll: function(search, replacement, target) {
        return target.split(search).join(replacement);
    },
    carregarDados: function(store, filtros, funcaoCallback) {
        store.clearFilter(true);

        for (var i in filtros) {
            store.filters.add(Ext.create('Ext.util.Filter', {
                property: filtros[i].property, //'id_comissao;=;int',
                value: filtros[i].value //1
            }));
        }

        store.load({
            callback: function(recs, operation, success) {
                if (funcaoCallback) {
                    funcaoCallback(recs);
                }
            }
        });
    },
    /* posicaoFinanceira: function(pclientes, pcliente=0, pcampo='id') {
         //pclientes será a store passada, pcliente é o record selecionado da store passada,
         //pcampo é o campo do record que identifica o id do cliente
         w = Ext.create('tecnobit.abstract.view.FormPosicaoCliente', {
             clientes: pclientes,
             cliente: pcliente,
             campo: pcampo
         });
     },
     formacaoPreco: function(pprodutos, pproduto=0, pcampo='id') {
         //pclientes será a store passada, pcliente é o record selecionado da store passada,
         //pcampo é o campo do record que identifica o id do cliente
         w = Ext.create('tecnobit.abstract.view.FormFormacaoPreco', {
             produtos: pprodutos,
             produto: pproduto,
             campo: pcampo
         });
     },*/
    /**
     * retorna a quantidade de dias
     */
    getDiffBetweenDates: function(d1, d2) {
        data = Ext.Date.getElapsed(new Date(d1), new Date(d2));
        diff = (data / (1000 * 60 * 60 * 24));

        if (this.comparaDatas(new Date(d1), new Date(d2)) == 'menor') {
            return diff * -1;
        }
        return diff;
    },

    /**
     * retorna se pe markup ou margem que esta configurado na formação de preço da empresa selecionada
     * @param pempresa {default 1}
     * @return bool
     */
    isMarkupConfigured: function(pempresa) {
        var me = this;
        return (me.getEmpresa(pempresa).formacao_preco_metodo == 1);
    },
    /*
    confirmationBox: function (args) {
        var msg = Ext.Msg.show({
            title: args.title,
            msg: args.msg,
            buttons: Ext.Msg.YESNO,
            icon: Ext.MessageBox.WARNING,
            scope: me._sender,
            width: 450,
            icon: Ext.MessageBox.WARNING,
            fn: args.fn
        });
        this.bringToFront(msg);
    }
    */
    /**
     * Exibe a tela para envio de arquivos
     * @param id
     * @param origem (util.dirFiles)
     */
    createWndUploadFiles: function(pid, porigem, renderToView, onlyImg, h, w) {
        var me = this;
        var conn = new Ext.data.Connection();
        if (tecnobit.Util.isEmpty(h)) h = 458;
        if (tecnobit.Util.isEmpty(w)) w = 458;
        conn.request({
            url: './php/varejoMaster/qrcode.php?action=getImg&id=' + pid + '&origem=' + porigem,
            method: 'GET',
            callback: function(cp, sucesso, resposta) {
                var obj = Ext.decode(resposta.responseText);
                if (obj.success) {
                    var wnd = Ext.create('Ext.window.Window', {
                        title: 'Mídias',
                        renderTo: renderToView,
                        animateTarget: Ext.getBody(),
                        constrainHeader: true,
                        constrain: true,
                        autoShow: true,
                        modal: false,
                        width: 707,
                        // height: 458,
                        items: [{
                            xtype: 'bitUploadFiles',
                            onlyImages: onlyImg,
                            config: {
                                id: pid,
                                tipo: porigem,
                                height: h,
                                width: w
                            }
                        }, {
                            xtype: 'panel',
                            height: 199,
                            html: [
                                "<tpl for='.'>",
                                "   <div style='display: flex; flex-direction: row; justify-content: flex-start; align-items: center'>",
                                "       <img src='" + obj.src + "'/>",
                                "       <div style='display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start'>",
                                "           <p style='margin-right: 20px;'>Utilize nosso aplicativo para efetuar o envio de fotos com a leitura deste QRCode</p>",
                                "           <p style='margin-right: 20px; font-size: 10px;'>Caso ainda não tenha instalado, clique nas opções abaixo e faça o download</p>",
                                "       </div>",
                                "       <div style='display: flex; flex-direction: column; align-items: center; justify-content: center;'>",
                                "           <a href='https://play.google.com/store/apps/details?id=com.varejomastermidia&hl=pt_BR' target='_blank'>",
                                "               <img src='../resources/images/google-play.png' style='max-width: 150px' />",
                                "           </a>",
                                "           <a href='https://itunes.apple.com/us/app/vm-m%C3%ADdias/id1337221985?mt=8' target='_blank'>",
                                "               <img src='../resources/images/app-store.png' style='max-width: 132px' />",
                                "           </a>",
                                "       </div>",
                                "   </div>",
                                "</tpl>"
                            ]
                        }]
                    });
                }
            }
        });
    },
    dirFiles: {
        pessoasdoc: 'pessoas/doc',
        pessoasfoto: 'pessoas/fotos',
        lancamentos: 'financeiro/lancamentos',
        produtos: 'produtos',
        orcamentos: 'orcamentos',
        os: 'os',
        vendas: 'vendas',
        pedidosLvDocumentos: 'pedidos/documentos'
    },
    /**
     * Retorna o QrCode para uso do aplicativo [Varejomaster Mídias]
     * @param id - id do registro dono da imagem
     * @param origem - tecnobit.Util.dirFiles.$
     * @param onlyImg - se TRUE vai retornar apenas o SRC da imagem, não vai abrir a janela com o QrCode
     * @param fcallback - se onlyImg = TRUE informar uma função de callback para pegar o SRC do QrCode
     */
    qrCode: function(id, origem, onlyImg, fcallback) {
        var me = this;
        var conn = new Ext.data.Connection();
        conn.request({
            url: './php/varejoMaster/qrcode.php?action=getImg&id=' + id + '&origem=' + origem,
            method: 'GET',
            callback: function(cp, sucesso, resposta) {
                var obj = Ext.decode(resposta.responseText);
                if (obj.success) {
                    if (!onlyImg) {
                        var wnd = Ext.create('Ext.window.Window', {
                            animateTarget: Ext.getBody(),
                            renderTo: Ext.getBody(),
                            height: 400,
                            width: 450,
                            title: 'Envio de fotos - QRCode',
                            constrainHeader: true,
                            constrain: true,
                            autoShow: true,
                            modal: false,
                            html: [
                                "<tpl for='.'>",
                                "   <div style='display: flex; flex-direction: column; margin-bottom: 10px; justify-content: center; align-items: center'>",
                                "       <img src='" + obj.src + "'/>",
                                "       <p style='margin-left: 20px; margin-right: 20px;'>Utilize nosso aplicativo para efetuar o envio de fotos com a leitura deste QRCode</p>",
                                "       <p style='margin-left: 20px; margin-right: 20px; font-size: 10px;'>Caso ainda não tenha instalado, clique nas opções abaixo e faça o download</p>",
                                "       <div style='margin-bottom: 10px; display: flex; flex-direction: column; align-items: center; justify-content: center;'>",
                                "           <a href='https://play.google.com/store/apps/details?id=com.varejomastermidia&hl=pt_BR' target='_blank'>",
                                "               <img src='../resources/images/google-play.png' style='max-width: 150px' />",
                                "           </a>",
                                "           <a href='https://itunes.apple.com/us/app/vm-m%C3%ADdias/id1337221985?mt=8' target='_blank'>",
                                "               <img src='../resources/images/app-store.png' style='max-width: 132px' />",
                                "           </a>",
                                "       </div>",
                                "   </div>",
                                "</tpl>"
                            ]
                        });
                        me.bringToFront(wnd);


                    }

                    if ((fcallback != undefined) && (typeof fcallback == 'function'))
                        fcallback(obj.src);
                }
            }
        });
    },
    objDadosPessoa: function(pDados) {
        var vDados = pDados.split("||");
        return {
            razao_social: vDados[0],
            cpf_cnpj: vDados[1],
            inscricao_estadual: vDados[2],
            insc_municipal: vDados[18],
            endereco: vDados[10],
            bairro: vDados[7],
            id_cidade: vDados[4],
            email: vDados[6],
            fone: vDados[8],
            nro: vDados[19],
            complemento: vDados[20],
            cep: vDados[21]
        };
    },
    getLogo: function() {
        return this.sessao.logo + "&nocache=" + Math.random();
    },
    dateJSToBr: function(date) {
        return this.formataDataBr(date.toISOString().slice(0, 10).split("-").reverse().join("/"));
    },
    fixarData: function(dias) {
        return new Date(new Date().getTime() + (dias) * 24 * 60 * 60 * 1000);
    },
    openWindowWithPost: function(url, windowoption, name, params) {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", url);
        form.setAttribute("target", name);
        for (var i in params) {
            if (params.hasOwnProperty(i)) {
                var input = document.createElement('input');
                input.type = 'hidden';
                input.name = i;
                input.value = params[i];
                form.appendChild(input);
            }
        }
        document.body.appendChild(form);
        window.open("post.htm", name, windowoption);
        form.submit();
        document.body.removeChild(form);
    }
});